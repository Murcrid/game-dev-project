﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThingTrigger : MonoBehaviour {

    [SerializeField]
    private GameObject gateOne, gateTwo;

    bool earlyFree;

    private void OnTriggerEnter(Collider other)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
        earlyFree = false;
        if (gateOne.activeInHierarchy)
        {
            gateOne.SetActive(false);
            gateTwo.SetActive(false);
        }
        else
        {
            StartCoroutine("ActiveLate");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        earlyFree = true;
        if (!gateOne.activeInHierarchy)
        {
            gateOne.SetActive(true);
            gateTwo.SetActive(true);
        }
    }

    private IEnumerable ActiveLate()
    {
        while (!gateOne.activeInHierarchy || earlyFree)
        {
            yield return new WaitForSeconds(0);
        }
        if (!earlyFree)
        {
            gateOne.SetActive(false);
            gateTwo.SetActive(false);
        }
    }

}
