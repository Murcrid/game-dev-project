﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Object Random Scaler randomly scales all objects in objects list with given values, 
/// or if objects list is empty it will search for all childs in hierarchy and scale them.
/// xAndZSizeOffset = Wanted percentage change in X and Z axises 
///     EG: 5 will give either 95% - 105% of original scale
/// ySizeOffset = Wanted percentage change in y axis 
///     EG: 5 will give either 95% - 105% of original scale
/// xAndZMedianOffset = Offset of the median of change. 
///     EG: 5 as media offset and 5 as size offset will result in 100% - 110% of original scale
/// yMedianOffset = Offset of the median of change. 
///     EG: 5 as media offset and 10 as size offset will result in 95% - 115% of original scale
/// iteration = The number of times these changes are applied to each object.
/// start = When set to true in the inspector, the code will run once and return this back to false state.
/// </summary>

[ExecuteInEditMode]
public class ObjectRandomScaler : MonoBehaviour {

    public List<GameObject> objects;

    [Range(1,100)] public int xAndZSizeOffset = 1, ySizeOffset = 1;
    [Range(-100, 100)] public int xAndZMedianOffset = 0;
    [Range(-100, 100)] public int yMedianOffset = 0;
    [Range(1, 10)] public int iterations = 1;
    public bool randomizeRotation = true;

    public bool start;
    public bool reset;

    private void Update()
    {
        if (start)
        {
            System.Random random = new System.Random();
            if (Mathf.Abs(xAndZMedianOffset) > xAndZSizeOffset || Mathf.Abs(yMedianOffset) > ySizeOffset)
            {
                Debug.LogError("Medians absolute values have to be less or equal to their corresponding offsets!");
                return;
            }
            for (int xi = 0; xi < iterations; xi++)
            {
                if (objects.Count > 0)
                {
                    for (int i = 0; i < objects.Count; i++)
                    {
                        float xAndz = (random.Next(-xAndZSizeOffset + xAndZMedianOffset, xAndZSizeOffset + xAndZMedianOffset + 1) + 100);
                        float y = (random.Next(-ySizeOffset + yMedianOffset, ySizeOffset + yMedianOffset + 1) + 100);

                        xAndz /= 100;
                        y /= 100;

                        objects[i].transform.localScale = new Vector3(
                            objects[i].transform.localScale.x * xAndz,
                            objects[i].transform.localScale.y * y,
                            objects[i].transform.localScale.z * xAndz);

                        if (randomizeRotation)
                            objects[i].transform.eulerAngles = new Vector3(0, random.Next(0, 360), 0);
                    }
                }
                else
                {
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        float xAndz = (random.Next(-xAndZSizeOffset + xAndZMedianOffset, xAndZSizeOffset + xAndZMedianOffset + 1) + 100);
                        float y = (random.Next(-ySizeOffset + yMedianOffset, ySizeOffset + yMedianOffset + 1) + 100);

                        xAndz /= 100;
                        y /= 100;

                        transform.GetChild(i).localScale = new Vector3(
                            transform.GetChild(i).localScale.x * xAndz,
                            transform.GetChild(i).localScale.y * y,
                            transform.GetChild(i).localScale.z * xAndz);

                        if (randomizeRotation)
                            transform.GetChild(i).eulerAngles = new Vector3(0, random.Next(0, 360), 0);
                    }
                }
            }        
            start = false;
        }
        if (reset)
        {
            if (objects.Count > 0)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    objects[i].transform.localScale = new Vector3(1, 1, 1);
                    objects[i].transform.eulerAngles = new Vector3(1, 1, 1);
                }
            }
            else
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).localScale = new Vector3(1, 1, 1);
                    transform.GetChild(i).eulerAngles = new Vector3(1, 1, 1);
                }
            }
            reset = false;
        }
    }
}
