﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTriggerController : MonoBehaviour {

    InventoryManager inventoryManager;
    PlayerRespawn pr;

    private void Start()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
        pr = GetComponent<PlayerRespawn>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coin"))
        {
            inventoryManager.Coins++;
            Destroy(other.gameObject);
        }
        else if(other.CompareTag("Respawn"))
            pr.Respawn();
    }
}
