﻿using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
public class SpawnPointTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        PlayerRespawn pr = other.GetComponent<PlayerRespawn>();
        if (!pr && pr.spawnPoint != transform)
            return;
        pr.spawnPoint = transform;
    }
}
