﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    [SerializeField]
    private int coinCount = 10;
    private InventoryManager inventoryManager;
    private Animator anim;
    private bool hasBeenOpened;

    private void Start()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
        anim = GetComponentInChildren<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !hasBeenOpened)
        {
            anim.SetTrigger("Open");
            inventoryManager.Coins += coinCount;
            hasBeenOpened = true;
        }
    }
}
