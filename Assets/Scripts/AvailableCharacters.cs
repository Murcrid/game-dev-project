﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvailableCharacters : MonoBehaviour {

    internal int SelectedCharacter { get; private set; }

    public enum CharacterType { Lily, Rain }

    [SerializeField] private Camera MainCamera;
    [SerializeField] private GameObject[] characters;

    [SerializeField]
    private List<GameObject> availableCharacters;
    [SerializeField]
    private UIPlayerController uiPlayerController;
    [SerializeField]
    private CameraMaster cameraMaster;

	// Use this for initialization
	void Start () {
        uiPlayerController = FindObjectOfType<UIPlayerController>();
        SelectedCharacter = -1;
        SwitchActiveCharacter();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab))
            SwitchActiveCharacter();
	}

    public void RemoveCharacter(CharacterType characterType)
    {
        switch (characterType)
        {
            case CharacterType.Rain:
                cameraMaster.RemoveCamera();
                uiPlayerController.RemoveCharacter(UIPlayerController.CharacterType.Rain);
                Destroy(characters[1]);
                SelectedCharacter = 0;
                UpdateControls();
                break;
            case CharacterType.Lily: 
                cameraMaster.RemoveCamera();
                uiPlayerController.RemoveCharacter(UIPlayerController.CharacterType.Lily);
                Destroy(characters[0]);
                SelectedCharacter = 1;
                UpdateControls();
                break;
        }
    }

    public void AddCharacter(CharacterType characterType, Vector3 spawnSpot)
    {
        switch (characterType)
        {
            case CharacterType.Rain:
                uiPlayerController.AddCharacter(UIPlayerController.CharacterType.Rain);
                characters[1] = (Instantiate(availableCharacters[1], spawnSpot, availableCharacters[1].transform.rotation));
                UpdateControls();
                break;
            case CharacterType.Lily:              
                uiPlayerController.AddCharacter(UIPlayerController.CharacterType.Lily);
                characters[0] = (Instantiate(availableCharacters[0], spawnSpot, availableCharacters[0].transform.rotation));
                UpdateControls();
                break;
        }
    }

    private void SwitchActiveCharacter()
    {
        if (SelectedCharacter < characters.Length - 1)
            SelectedCharacter++;
        else
            SelectedCharacter = 0;
        UpdateControls();
    }

    private void UpdateControls()
    {
        for (int i = 0; i < characters.Length; i++)
        {
            if (characters[i] != null)
            {
                if (i == SelectedCharacter)
                {
                    characters[i].GetComponent<PlayerController>().controlsEnabled = true;
                    cameraMaster.SetNewTarget(characters[i]);
                    uiPlayerController.UpdateVisuals(i);
                }
                else
                    characters[i].GetComponent<PlayerController>().controlsEnabled = false;
            }
        }
    }
}
