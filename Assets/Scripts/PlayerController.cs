﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private float walkSpeed = 8.0f;
    [SerializeField] private float crawlSpeed = 3.0f;
    [SerializeField] private float jumpSpeed = 8.0f;
    [SerializeField] private float gravity = 20.0f;
    [SerializeField] private float rotateSpeed = 3.0f;
    [SerializeField] private float airControlSpeed = 3.0f;
    [SerializeField] private float standUpHeight = 2.3f;
    [SerializeField] private float crouchHeight = 1.0f;
    private float speed;
    public bool isCrawling = false;
    public bool controlsEnabled = true;
    private bool isInCrawlArea = false;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 jumpVelocity = Vector3.zero;
    private CharacterController controller;

    private CharacterStatus characterStatus;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        characterStatus = GetComponent<CharacterStatus>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isInCrawlArea)
        {
            //speed = walkSpeed;
            LilyMovement();
        }
        /*
        isCrawling = true;
        speed = crawlSpeed;
        LilyCrawl();
        */

    }

    void LilyMovement()
    {
        if (controlsEnabled)
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                Crouch();
            }
            else
            {
                if (characterStatus.AbleToStand)
                {
                    StandUp();
                }
            }

            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
            moveDirection = transform.TransformDirection(moveDirection);



            if (controller.isGrounded)
            {
                moveDirection *= walkSpeed;
                
                if (characterStatus.Crouching)
                {
                    moveDirection *= crawlSpeed;
                }
                else if (!characterStatus.Crouching)
                {
                    moveDirection *= walkSpeed;
                }

                if (Input.GetButton("Jump"))
                {
                    jumpVelocity = moveDirection;
                    jumpVelocity.y = jumpSpeed;
                }
                else
                {
                    jumpVelocity = Vector3.zero;
                }
            }
            else
            {
                moveDirection *= airControlSpeed;
                jumpVelocity.y -= gravity * Time.deltaTime;
            }

            controller.Move((moveDirection + jumpVelocity) * Time.deltaTime);

            
        }
        else
        {
            
            moveDirection.y -= gravity * Time.deltaTime;
            Vector3 dir = new Vector3(0, moveDirection.y, 0);
            controller.Move(dir * Time.deltaTime);
        }       
    }

    void Crouch()
    {
        
        characterStatus.Crouching = true;
        controller.height = crouchHeight;
        controller.center = new Vector3(0, 0.5f, 0);
        
    }

    void StandUp()
    {
        characterStatus.Crouching = false;
        controller.height = standUpHeight;
        controller.center = new Vector3(0, 1, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "crawlArea")
        {
            isInCrawlArea = true;
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "crawlArea")
        {
            isInCrawlArea = false;
        }
    }
}
