﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private bool isRotating = true;
    [SerializeField] private bool randomizedRotation = true;
    [SerializeField] private float rotateSpeed = 20;

    [SerializeField] private bool isMoving;
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform patrolSpotOne, patrolSpotTwo;

    private System.Random random = new System.Random();
    private bool rotDir;
    private bool moveDir;

    private void Start()
    {
        patrolSpotOne.SetParent(GameObject.FindGameObjectWithTag("PatrolSpotParent").transform);
        patrolSpotTwo.SetParent(GameObject.FindGameObjectWithTag("PatrolSpotParent").transform);
    }

    private void Update()
    {
        if (isRotating)
        {
            if (random.Next(0, 1000) == 1 && randomizedRotation)
                rotDir = !rotDir;
            if (rotDir)
                transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
            else
                transform.Rotate(Vector3.up * -rotateSpeed * Time.deltaTime);
        }
        if (isMoving)
        {
            if (moveDir)
            {
                transform.Translate((patrolSpotOne.position - transform.position).normalized * Time.deltaTime * moveSpeed, Space.World);
                transform.LookAt(patrolSpotOne);
                if (Vector3.Distance(transform.position, patrolSpotOne.position) < 1)
                    moveDir = false;
            }
            else
            {
                transform.Translate((patrolSpotTwo.position - transform.position).normalized * Time.deltaTime * moveSpeed, Space.World);
                transform.LookAt(patrolSpotTwo);
                if (Vector3.Distance(transform.position, patrolSpotTwo.position) < 1)
                    moveDir = true;
            }       
            
        }
    }

    internal void Alert(GameObject foundTarget)
    {
        if (foundTarget.tag == "Player")
            foundTarget.GetComponent<PlayerRespawn>().Respawn();
    }

}
