﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatus : MonoBehaviour {
    //0 for backwards, 1 for forwards, 2 for false
    internal int Moving { get; set; }
    internal bool Crouching { get; set; }
    internal bool Jumping { get; set; }
    internal bool Falling { get; set; }
    internal bool AbleToStand { get; set; }
    //0 for left, 1 for right, 2 for false
    internal int Turning { get; set; }

    [SerializeField] private float upRayLength = 3.0f;

    CharacterController controller;
    // Use this for initialization
    void Start () {
        controller = gameObject.GetComponentInParent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Crouching)
        {
            CheckIfAbleToStand();
        }
        if (controller.isGrounded)
        {
            Jumping = false;
            Falling = false;
            if ((controller.velocity.x > 0.5 || controller.velocity.z > 0.5 || controller.velocity.x < -0.5 || controller.velocity.z < -0.5) && (controller.velocity.y < 2 && controller.velocity.y > -2))
            {
                Moving = 1;
            }
            else
            {
                Moving= 2;
            }


        }
        if (!controller.isGrounded)
        {
            Moving = 2;
            if (controller.velocity.y > 0.5)
            {
                Jumping = true;
            }
            else
            {
                Jumping = false;
            }
            if (controller.velocity.y < -0.5)
            {
                Falling = true;
            }
            else
            {
                Falling = false;
            }

        }
    }

    void CheckIfAbleToStand()
    {
        
       
            int layerMask = 1 << 9;


            layerMask = ~layerMask;

            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out hit, upRayLength, layerMask))
            {
                AbleToStand = false;
            }
            else
            {
                AbleToStand = true;
            }
        
    }
}
