﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainAnimationStatus : MonoBehaviour {

    CharacterController controller;
    Animator animator;
    private CharacterStatus characterStatus;
	// Use this for initialization
	void Start () {
        characterStatus = gameObject.GetComponentInParent<CharacterStatus>();
        controller = gameObject.GetComponentInParent<CharacterController>();
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        animator.SetInteger("Moving", characterStatus.Moving);
        animator.SetInteger("Turning", characterStatus.Turning);
        animator.SetBool("Crouching", characterStatus.Crouching);
        animator.SetBool("Jumping", characterStatus.Jumping);
        animator.SetBool("Falling", characterStatus.Falling);
    }
}
