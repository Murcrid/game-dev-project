﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour {

    public void PlayButtonClick()
    {
        SceneManager.LoadScene(2);
    }
    public void OptionsButtonClick()
    {

    }
    public void ExitButtonClick()
    {
        Application.Quit();
    }
}
