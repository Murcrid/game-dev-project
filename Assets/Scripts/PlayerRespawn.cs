﻿using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    internal Transform spawnPoint;

    public void Respawn()
    {
        transform.position = spawnPoint.position;
    }
}
