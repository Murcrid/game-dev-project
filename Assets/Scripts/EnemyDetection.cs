﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour {

    [SerializeField] private Enemy e;

    private void OnTriggerEnter(Collider other)
    {
        e.Alert(other.gameObject);     
    }
}
