﻿using UnityEngine;


public class InventoryManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public static InventoryManager instance;

    private MoneyCounter moneyCounter;
    private static int _Coins;

    internal int Coins { get { return _Coins; } set { _Coins = value; moneyCounter.UpdateMoney(_Coins); } }

    public void Init()
    {
        moneyCounter = FindObjectOfType<MoneyCounter>();
        Coins = _Coins;
    }
}