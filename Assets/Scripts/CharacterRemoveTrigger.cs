﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRemoveTrigger : MonoBehaviour {

    public enum Characters { Lily, Rain }

    [SerializeField]
    private Characters charToRemove;

    private AvailableCharacters avc;

    private void Start()
    {
        avc = FindObjectOfType<AvailableCharacters>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (charToRemove)
            {
                case Characters.Lily:
                    avc.RemoveCharacter(AvailableCharacters.CharacterType.Lily);
                    break;
                case Characters.Rain:
                    avc.RemoveCharacter(AvailableCharacters.CharacterType.Rain);
                    break;
            }
            Destroy(gameObject);
        }
    }
}
