﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMaster : MonoBehaviour {

    private GameObject target = null;
    private Vector3 offset = new Vector3(0, 5, 6);

    public void SetNewTarget(GameObject newTarget)
    {
        transform.SetParent(newTarget.transform);
        transform.position = newTarget.transform.GetChild(1).position;
        transform.LookAt(newTarget.transform);
    }

    public void RemoveCamera()
    {
        transform.parent = null;
    }

}
