﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }
    public static GameManager instance;

    public void LoadLobby()
    {
        SceneManager.LoadScene(1);
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 0)
            SceneManager.LoadScene(1);
        else if(scene.buildIndex == 2)
        {
            InitManager();
            FindObjectOfType<AvailableCharacters>().AddCharacter(AvailableCharacters.CharacterType.Lily, new Vector3(35, 10, 300));
        }
    }

    private void InitManager()
    {
        FindObjectOfType<InventoryManager>().Init();
    }
}
