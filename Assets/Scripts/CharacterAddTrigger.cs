﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAddTrigger : MonoBehaviour {

	public enum Characters { Lily, Rain }

    [SerializeField]
    private Characters charToAdd;
    [SerializeField]
    private Vector3 spawnPosition;
    [SerializeField]
    private bool multiTriggerable;
    [SerializeField]
    private bool useMyPos;

    private AvailableCharacters avc;

    private void Start()
    {
        avc = FindObjectOfType<AvailableCharacters>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (charToAdd)
            {
                case Characters.Lily:
                    avc.AddCharacter(AvailableCharacters.CharacterType.Lily, useMyPos ? transform.position : spawnPosition);
                    break;
                case Characters.Rain:
                    avc.AddCharacter(AvailableCharacters.CharacterType.Rain, useMyPos ?  transform.position : spawnPosition);
                    break;
            }
            if (!multiTriggerable)
                Destroy(gameObject);
        }
    }
}
