﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIPlayerController : MonoBehaviour {

    [SerializeField]
    private List<GameObject> availableChars;

    private GameObject[] chars = new GameObject[2];

    public enum CharacterType { Rain, Lily }

    private Vector2 bigSize = new Vector2(330, 150);
    private Vector2 smallSize = new Vector2(155, 75);
    private Vector2 bigSubSize = new Vector2(150, 0);
    private Vector2 smallSubSize = new Vector2(75, 0);

    public void RemoveCharacter(CharacterType characterType)
    {
        switch (characterType)
        {
            case CharacterType.Rain:
                Destroy(chars[1]);              
                UpdateVisuals(0);
                break;
            case CharacterType.Lily:
                Destroy(chars[0]);
                UpdateVisuals(0);
                break;
        }
    }

    public void AddCharacter(CharacterType characterType)
    {
        switch (characterType)
        {
            case CharacterType.Rain:
                chars[1] = (Instantiate(availableChars[1], transform));
                UpdateVisuals(1);
                break;
            case CharacterType.Lily:
                chars[0] = (Instantiate(availableChars[0], transform));
                UpdateVisuals(0);
                break;
        }
    }

    public void UpdateVisuals(int index)
    {
        chars[index].transform.SetAsFirstSibling();
        AdjustSizes();
    }

    private void AdjustSizes()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if(i == 0)
            {
                transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = bigSize;
                for (int x = 0; x < chars[i].transform.childCount; x++)
                {
                    transform.GetChild(i).GetChild(x).GetComponent<RectTransform>().sizeDelta = bigSubSize;
                }
            }    
            else
            {
                transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = smallSize;
                for (int x = 0; x < chars[i].transform.childCount; x++)
                {
                    transform.GetChild(i).GetChild(x).GetComponent<RectTransform>().sizeDelta = smallSubSize;
                }
            }
        }
    }
}
