﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogTrigger : MonoBehaviour {

    public ConversationData conversationToTrigger;
    public bool multitriggerable = false;
    public enum Characters { Lily, Rain }

    public bool activeForCharacterOnly;
    public Characters characterToActiveOn;
    public bool requiresBothCharacters;

    internal bool conversationTriggered { get; private set; }

    private bool lilyFound, rainFound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && (!conversationTriggered || multitriggerable))
        {
            if (requiresBothCharacters && other.transform.name == "Lily(Clone)" && !lilyFound)
            {
                lilyFound = true;
            }
            else if (requiresBothCharacters && other.transform.name == "Rain(Clone)" && !rainFound)
            {
                rainFound = true;
            }
            else
            {
                if (activeForCharacterOnly)
                {
                    if (other.transform.name == "Rain(Clone)" && characterToActiveOn == Characters.Rain)
                    {
                        FindObjectOfType<UIDialog>().InitializeConversation(conversationToTrigger);
                        conversationTriggered = true;
                    }
                    else if (other.transform.name == "Lily(Clone)" && characterToActiveOn == Characters.Lily)
                    {
                        FindObjectOfType<UIDialog>().InitializeConversation(conversationToTrigger);
                        conversationTriggered = true;
                    }
                }
                else
                {
                    FindObjectOfType<UIDialog>().InitializeConversation(conversationToTrigger);
                    conversationTriggered = true;
                }
            }
            if (lilyFound && rainFound)
            {
                FindObjectOfType<UIDialog>().InitializeConversation(conversationToTrigger);
                conversationTriggered = true;
            }
        }
    }
}
