﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DialogData  {

    public enum Character { Lily, Rain }
    public Character speaker;
    public string conversationText;

}
