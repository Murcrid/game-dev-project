﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIDialog : MonoBehaviour
{
    private ConversationData myConversation;
    private int conversationIndex = 0;

    [SerializeField] private TextMeshProUGUI body;
    [SerializeField] private TextMeshProUGUI header;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI nextButtonText;

    [SerializeField] private Sprite lilyPortrait, rainPortrait;

    public void InitializeConversation(ConversationData conversation)
    {
        myConversation = conversation;
        transform.GetChild(0).gameObject.SetActive(true);
        UpdateFields();
    }

    public void NextButtonClick()
    {
        if (conversationIndex < myConversation.data.Length - 2)
        {
            conversationIndex++;
            UpdateFields();
        }
        else if(conversationIndex < myConversation.data.Length - 1)
        {
            nextButtonText.text = "Close";
            conversationIndex++;
            UpdateFields();
        }
        else if(conversationIndex < myConversation.data.Length)
        {
            myConversation = null;
            conversationIndex = 0;
            nextButtonText.text = "Next";
            transform.GetChild(0).gameObject.SetActive(false);
            if (myConversation.isFinal)
            {
                FindObjectOfType<GameManager>().LoadLobby();
            }
        }
    }

    public void BackButtonClick()
    {
        if (conversationIndex > 0)
        {
            nextButtonText.text = "Next";
            conversationIndex--;
            UpdateFields();
        }
    }

    private void UpdateFields()
    {
        body.text = myConversation.data[conversationIndex].conversationText;
        header.text = myConversation.data[conversationIndex].speaker.ToString();
        switch (myConversation.data[conversationIndex].speaker)
        {
            case DialogData.Character.Lily:
                image.sprite = lilyPortrait;
                break;
            case DialogData.Character.Rain:
                image.sprite = rainPortrait;
                break;
        }
        
    }
}
