﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyCounter : MonoBehaviour {

    TextMeshProUGUI moneyText;

	void Start () {
        moneyText = GetComponentInChildren<TextMeshProUGUI>();
	}
	

    public void UpdateMoney(int newValue)
    {
        if(moneyText)
            moneyText.text = newValue.ToString();
    }
}
