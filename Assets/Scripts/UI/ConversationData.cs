﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ConversationData : ScriptableObject
{
    public DialogData[] data;
    public bool isFinal;
}
