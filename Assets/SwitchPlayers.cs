﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPlayers : MonoBehaviour {
    [SerializeField]
    private GameObject lili;
    [SerializeField]
    private GameObject rain;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (lili.activeSelf)
            {
                rain.GetComponent<Transform>().position = lili.GetComponent<Transform>().position;
                rain.GetComponent<Transform>().rotation = lili.GetComponent<Transform>().rotation;

                lili.SetActive(false);
                rain.SetActive(true);
            }
            else
            {
                lili.GetComponent<Transform>().position = rain.GetComponent<Transform>().position;
                lili.GetComponent<Transform>().rotation = rain.GetComponent<Transform>().rotation;

                rain.SetActive(false);
                lili.SetActive(true);
            }
        }
    }
}
